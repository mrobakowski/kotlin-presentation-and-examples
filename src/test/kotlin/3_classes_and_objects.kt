@file:Suppress("unused")

class Baz // klasy nie muszą mieć ciała, jeśli nie jest potrzebne...

// singleton!
object Foo {
    init {

    }
}

fun makeMeSomeJSONs() {
    val x1 = object {
        val a = 1
        val b = "foo"
    }
    x1.a
    json(x1)

    json(object {
        val x = 3.5
    })
}

interface Factory<out T> {
    fun getInstance(): T?

    fun foo() = 42
}

class Bar {
    fun meaningOfLife() = 42

    // companion object zamiast metod statycznych, są pełnoprawnymi obiektami, mogą dziedziczyć
    companion object : Factory<Bar> {
        override fun getInstance() = if (Math.random() > 0.5) Bar() else null
    }
}

fun createAndPrint(f: Factory<*>) = println(f.getInstance())

fun `companion objecty mogą być normalnie przekazywane jak każde inne obiekty`() {
    createAndPrint(Bar.Companion)
}

class A(a: Int = 7, b: Int) { // primary constructor
    constructor() : this(b = 2) // { // secondary constructor, ciało jest opcjonalne
    //      ... some more init stuff ...
    // }

    // domyślnie wszystko jest publiczne
    val aa = a
    var bb = b

    // customowe gettery i settery
    var cc: Int
        get() = aa
        set(value) {
            bb = value
        }

    var x = "Foo"
        private set // ograniczenie tylko na setterze
}

class B(val a: Int, var b: String)

data class C(val a: Int, var b: String)

fun main(args: Array<String>) {
    println(B(1, "foo"))
    println(C(2, "bar"))

    val c = C(1, "foo")

    val d = c.copy(a = 42, b = "foobar")
    // data klasy mają domyślnie zaimplementowane toString, equals, hashCode, oraz funkcje do "multideklaracji"

    val (a, b) = d
    // to samo co:
//    val a = d.component1()
//    val b = d.component2()
}