@file:Suppress("unused", "UNUSED_VARIABLE", "IfThenToSafeAccess", "ReplaceSingleLineLet", "ConvertLambdaToReference")

fun `typy nullowalne i nienullowalne`() {
    // typy nullowalne mają '?' na końcu
    val thisMayBeABarOrANull: Bar? = Bar.getInstance()

//    thisMayBeABarOrANull.meaningOfLife() // <- to jest błąd, w Javie potencjalny NPE

    // safe access expression x ?. y
    val a = thisMayBeABarOrANull?.meaningOfLife() // x: Int?

    // elvis operator x ?: y (można skojarzyć z operatorem ternarnym bez środkowej części)
    val b = a ?: 0 // b: Int

    if (a != null) {
        // typesystem wie, że tutaj a: Int, nie 'Int?'
        a.inc()
    } else {
        // tutaj a: Int?
        foo(a)
    }

    // ... ale!
    class Foo(var x: Int?) {
        fun test() {
            if (x != null) {
//                 x.inc()
                // Error: Smart cast to 'Int' is impossible, because 'x' is mutable property that could have been
                //        changed by this time
            }
            // rozwiązanie
            val x = x
            if (x != null) {
                x.inc()
            }
            // lub
            this.x?.let { x ->
                x.inc()
            }

            // lub...
            this.x!!.inc() // rzuć NPE, I don't care
        }
    }

    thisMayBeABarOrANull?.let { thisCannotBeNull -> // if (thisMayBeNull != null) { ...
        println("The meaning of life: ${thisCannotBeNull.meaningOfLife()}") // interpolacja tekstu
    } ?: run { // } else {
        println("Life has no meaning")
    }
}
