
// kompiluje się albo do jednego interfejsu (jeśli target = JVM8), albo tak jak trait w Scali (interfejs + klasa na domyślne implementacje)
interface SingleInterface {
    val bar: String
    fun foo() = 42
}